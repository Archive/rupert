#include <gnome.h>
#include <config.h>
#include "tetris.h"
#include <gdk/gdkkeysyms.h>

gint
is_blocked (TetrisMove move)
{

  int new_x = 0, x = 4, right, left, temp_x;
  gint i;
  gint y = 4;
  gint temp_y;
  switch (move)
    {
    case DOWN:

      do
	{
	  y--;

	  for (i = 0; i < 4; i++)
	    {
	      if (cur_piece[y][i] != NULL)
		break;
	    }
	}
      while ((i == 4) && (y >= 0));
      for (i = 0; i < 4; i++)
	{
	  temp_y = y;
	  while ((cur_piece[temp_y][i] == NULL) && (temp_y > 0))
	    temp_y--;
	  if ((cur_y + y + temp_y + 1) > 19)
	    break;
	  if ((cur_piece[temp_y][i] != NULL) && (pit[cur_y + temp_y + 1][cur_x + i] != NULL))
	    {
	      break;
	    }
	  else if ((cur_y + y) == 19)
	    break;
	}
      if (i == 4)
	return FALSE;
      else
	return TRUE;
      break;			/* for the switch */
    case LEFT:
    case RIGHT:
      if (move == LEFT)
	new_x = cur_x - 1;
      else if (move == RIGHT)
	new_x = cur_x + 1;
      do
	{
	  x--;
	  for (i = 0; i < 4; i++)
	    {
	      if (cur_piece[i][x] != NULL)
		break;
	    }
	}
      while ((i == 4) && (x >= 0));

      left = i;
      x = 4;

      do
	{
	  x--;
	  for (i = 3; i > -1; i--)
	    {
	      if (cur_piece[i][x] != NULL)
		break;
	    }
	}
      while ((i == -1) && (x >= 0));

      right = i;

      if ((new_x < (0 - left)) || (new_x > (9 + right)))
	return TRUE;
      if (new_x == (cur_x - 1))
	{

	  x = -1;
	  do
	    {
	      x++;
	      for (i = 0; i < 4; i++)
		{
		  if (cur_piece[i][x] != NULL)
		    break;
		}
	    }
	  while ((i == 4) && (x < 4));

	  if (pit[cur_y + i][cur_x + x - 1] != NULL)
	    return TRUE;
	  for (i = 0; i < 4; i++)
	    {

	      temp_x = x;
	      while ((cur_piece[i][temp_x] == NULL) && (temp_x < 4))
		temp_x++;
	      if ((cur_piece[i][temp_x] != NULL) && (pit[cur_y + i][cur_x + temp_x - 1] != NULL))
		return TRUE;
	    }
	}
      else if (new_x == (cur_x + 1))
	{
	  x = 4;
	  do
	    {
	      x--;
	      for (i = 0; i < 4; i++)
		{
		  if (cur_piece[i][x] != NULL)
		    break;
		}
	    }
	  while ((i == 4) && (x > -1));
	  if (pit[cur_y + i][cur_x + x + 1] != NULL)
	    return TRUE;
	  for (i = 0; i < 4; i++)
	    {
	      temp_x = x;
	      while ((cur_piece[i][temp_x] == NULL) && (temp_x > 0))
		temp_x--;
	      if ((cur_piece[i][temp_x] != NULL) && (pit[cur_y + i][cur_x + temp_x + 1] != NULL))
		return TRUE;
	    }
	}
      return FALSE;
      break;			/* for the switch */
    }
  return TRUE;
}

void
drop_to_bottom ()
{
  while (!(is_blocked (DOWN)))
    {
      update_pit (DOWN);
    }
  gnome_canvas_update_now (GNOME_CANVAS (canvas));
  usleep (250000);
  load_theme ();
}

gint
keyboard_cb (GtkWidget * widget, GdkEventKey * event, gpointer data)
{
  int new_x = 0;

  switch (event->keyval)
    {
    case GDK_Left:
    case GDK_h:
    case GDK_H:
    case GDK_KP_Left:
    case GDK_KP_4:
      new_x = cur_x - 1;
      break;
    case GDK_Right:
    case GDK_L:
    case GDK_l:
    case GDK_KP_Right:
    case GDK_KP_6:
      new_x = cur_x + 1;
      break;
    case GDK_J:
    case GDK_j:
    case GDK_KP_Down:
    case GDK_KP_2:
    case GDK_Down:
    case GDK_space:
      drop_to_bottom ();
      return TRUE;
      break;
    default:
      return FALSE;
    }

  if (new_x == (cur_x - 1))
    {
      if (!(is_blocked (LEFT)))
	update_pit (LEFT);
    }
  else if (new_x == (cur_x + 1))
    {
      if (!(is_blocked (RIGHT)))
	update_pit (RIGHT);
    }
  return TRUE;
}

int
timeout_cb (GtkWidget * widget, gpointer data)
{
  if (is_blocked (DOWN))
    {
      update_pit (STAY);
      load_theme ();
    }
  else
    {
      update_pit (DOWN);
    }
  clean_up_pit();
  return TRUE;
}
void
update_pit (TetrisMove move)
{
  gint x = 0;
  gint xoff = 0;		/* offsets */
  gint yoff = 0;
  gint y = 0;

  if (is_blocked (move))
    return;			/* just in case */
  switch (move)
    {
    case LEFT:
      xoff = -1;
      break;
    case RIGHT:
      xoff = 1;
      break;
    case DOWN:
      yoff = 1;
      break;
    default:
      break;
    }

  for (x = 3; x >= 0; x--)
    {
      for (y = 3; y >= 0; y--)
	{
	  if (((cur_y + y + yoff) < 20) && ((cur_x + x + xoff) < 10) && (cur_piece[y][x]))
	    {
	      pit[cur_y + y][cur_x + x] = NULL;
	      pit[cur_y + y + yoff][cur_x + x + xoff] = cur_piece[y][x];
	    }
	}
    }
  gnome_canvas_item_move (GNOME_CANVAS_ITEM (cur_group),
			  (double) (xoff * 16.0), (double) (yoff * 16.0));
  cur_x += xoff;
  cur_y += yoff;
}

void
clean_up_pit ()
{
  int i, x, y;
  for (i = 0; i < 4; i++)
    {
      for (y = 19; y >= 0; y--)
	{
	  for (x = 0; x < 10; x++)
	    {
	      if (pit[y][x] == NULL)
		break;
	    }
	  if (x == 10)
	    break;
	}
      for (; y > 0; y--)
	{
	  pit[y] = pit[y - 1];
	}
      pit[y] = NULL;
      update_pit (STAY);
    }
}
