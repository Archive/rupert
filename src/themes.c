#include <gnome.h>
#include <config.h>
#include "tetris.h"

gint
check_themes ()
{
  return TRUE;
}


void
load_theme ()
{
  gint x, y;
  GdkImlibImage *image;
  GnomeCanvasItem **tmp;

  cur_piece = g_new (GnomeCanvasItem **, 4);

  image = gdk_imlib_load_image ("block.png");
  cur_group = GNOME_CANVAS_GROUP (gnome_canvas_item_new (root,
			 gnome_canvas_group_get_type (), "x", 0.0, "y", 0.0,
							 NULL));

  for (y = 0; y < 4; y++)
    {
      tmp = g_new (GnomeCanvasItem *, 4);
      for (x = 0; x < 4; x++)
	{
	  tmp[x] = NULL;
	}
      cur_piece[y] = tmp;
    }

  for (x = 0; x < 4; x++)
    for (y = 0; y < 4; y++)
      cur_piece[y][x] = NULL;

  /*cur_piece[0][2] = create_new_block (image, 96, 0);
  cur_piece[1][1] = create_new_block (image, 80, 16);
  cur_piece[1][2] = create_new_block (image, 96, 16);
  cur_piece[0][3] = create_new_block (image, 112, 0);*/
  cur_piece[0][0] = create_new_block (image, 0, 0);
  cur_piece[1][0] = create_new_block (image, 0, 16);
  cur_piece[2][0] = create_new_block (image, 0, 32);
  cur_piece[3][0] = create_new_block (image, 0, 48);
	
  cur_x = 0;
  cur_y = 0;

  if (is_blocked (DOWN))
    {
      g_print ("You lose. sorry. \n");
      gtk_main_quit ();
    }
  update_pit (STAY);

}

GnomeCanvasItem *
create_new_block (GdkImlibImage * image, double x, double y)
{
  GnomeCanvasItem *block;
  block = gnome_canvas_item_new (cur_group, gnome_canvas_image_get_type (),
			    "image", image, "x", x, "y", y, "width",
				 (double) image->rgb_width, "height",
		    (double) image->rgb_height, "anchor", GTK_ANCHOR_CENTER,
				 NULL);

  return block;
}
