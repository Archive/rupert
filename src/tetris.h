#ifndef __TETRIS_H__
#define __TETRIS_H__

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include <gnome.h>
#include <gdk_imlib.h>
#include <config.h>


typedef enum 
{
	LEFT,
	RIGHT,
	STAY,
	DOWN
} TetrisMove;
	

GtkWidget *app, *canvas;
GnomeCanvasGroup *root;
GnomeCanvasItem ***pit;
GnomeCanvasItem ***cur_piece;
GnomeCanvasGroup *cur_group;
gint cur_x, cur_y;		/* this corresponds to cur_piece[0][0] */

int timeout_cb (GtkWidget * widget, gpointer data);
void create_main_window ();
GnomeCanvasItem *create_new_block (GdkImlibImage * image, double x, double y);
gint check_themes ();
void quit_cb (GtkWidget * widget, gpointer data);
void init_canvas ();
void load_theme ();
void update_pit (TetrisMove move);
gint keyboard_cb (GtkWidget * widget, GdkEventKey * event, gpointer data);
void drop_to_bottom ();
void clean_up_pit();
#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __TETRIS_H__ */

