#include <gnome.h>
#include <config.h>
#include <gdk_imlib.h>
#include <argp.h>
#include "tetris.h"
#include <unistd.h>

struct argp parser =
{NULL, NULL, NULL, NULL, NULL, NULL, NULL};

int
main (int argc, char *argv[])
{
  gnome_init ("tetris", &parser, argc, argv, 0, NULL);
  create_main_window ();
  gtk_timeout_add (300, GTK_SIGNAL_FUNC (timeout_cb), NULL);
  gtk_main ();
  return 0;
}


void
create_main_window ()
{
  GtkWidget *vbox;
  GtkWidget *hbox;
  GtkWidget *button;
  GtkWidget *label;
  GtkWidget *frame;

  if (check_themes ())
    {
      app = gnome_app_new ("tetris", "Tetris");
      gtk_signal_connect (GTK_OBJECT (app), "delete_event",
			  GTK_SIGNAL_FUNC (quit_cb), NULL);
      gtk_signal_connect (GTK_OBJECT (app), "key_press_event",
			  GTK_SIGNAL_FUNC (keyboard_cb), 0);
      init_canvas ();
      frame = gtk_frame_new ("");
      gtk_container_add (GTK_CONTAINER (frame), canvas);
      vbox = gtk_vbox_new (0, 5);
      hbox = gtk_hbox_new (0, 0);
      gtk_box_pack_start (GTK_BOX (vbox), frame, FALSE, FALSE, 5);
      gtk_box_pack_start (GTK_BOX (hbox), vbox, FALSE, FALSE, 5);
      gnome_app_set_contents (GNOME_APP (app), hbox);
      gtk_widget_set_usize (GTK_WIDGET (app), 175, 335);
    }
  else
    {
      app = gnome_app_new ("tetris", "Error");
      gtk_signal_connect (GTK_OBJECT (app), "delete_event",
			  GTK_SIGNAL_FUNC (quit_cb), NULL);
      vbox = gtk_vbox_new (FALSE, 5);
      hbox = gtk_hbox_new (FALSE, 0);
      label = gtk_label_new ("Fatal Error: \n Could not find any theme files in foo.");
      button = gtk_button_new_with_label ("Close");
      gtk_signal_connect (GTK_OBJECT (button), "clicked",
			  GTK_SIGNAL_FUNC (quit_cb), NULL);
      gtk_box_pack_start (GTK_BOX (vbox), label, FALSE, FALSE, 5);
      gtk_box_pack_end (GTK_BOX (hbox), button, FALSE, FALSE, 5);
      gtk_box_pack_start (GTK_BOX (vbox), hbox, FALSE, FALSE, 0);
      gnome_app_set_contents (GNOME_APP (app), vbox);
    }
  gtk_widget_show_all (app);
}


void
quit_cb (GtkWidget * widget, gpointer data)
{
  gtk_main_quit ();
}

void
init_canvas ()
{
  gint x, y;
  GnomeCanvasItem **row;

  pit = g_new (GnomeCanvasItem **, 20);
  gtk_widget_push_visual (gdk_imlib_get_visual ());
  gtk_widget_push_colormap (gdk_imlib_get_colormap ());

  canvas = gnome_canvas_new ();

  gtk_widget_pop_visual ();
  gtk_widget_pop_colormap ();
  gnome_canvas_set_pixels_per_unit (GNOME_CANVAS (canvas), 1.0);
  gtk_widget_set_usize (canvas, 160, 320);
  gnome_canvas_set_scroll_region (GNOME_CANVAS (canvas), 0, 0, 160,
				  320);
  root = GNOME_CANVAS_GROUP (gnome_canvas_root (GNOME_CANVAS (canvas)));

  for (y = 0; y < 20; y++)
    {
      row = g_new (GnomeCanvasItem *, 10);
      for (x = 0; x < 10; x++)
	{
	  row[x] = NULL;
	}
      pit[y] = row;
    }


  load_theme ();
}
